# README #

Normally, this README would detail the procedures required to get your application up and running.

### What is the purpose of this repository? 
A webapp to help the DE's day-to-day work.

* Realtime hitrate calculator 
* Realtime Sitemap finding 
* Create cache-apis from json dumps 
* Realtime ETA calculator (in development) 
* Convert large integers to comapct form

### Quick start 
* Check that you have Python 3.9 installed.
* Install all of the project's dependencies from requirements.txt.
* To install dependencies, use `pip install -r requirements.txt`. 
* Run the app.py file to activate the webapp:`streamlit run app.py`
* Default Local URL: `http://localhost:8501` 

