import streamlit as st
import re
import json
import requests
from math import floor
from datetime import datetime, date, timedelta
from urllib.parse import unquote
from lxml import html
from googlesearch import search

today = datetime.now()



st.set_page_config(page_title="DataWeave - Hit Rate Calculator")

add_selectbox = st.sidebar.selectbox(
    "Which tool do you want to use?",
    ("Hitrate","ETA","Cache API Generator","Count ⇆ Number","URL Decoder","Sitemap Finder")
)

def convert_str_to_number(x):
    total_stars = 0
    num_map = {'K':1000, 'M':1000000, 'B':1000000000, 'T':1000000000000}
    if x.isdigit():
        total_stars = int(x)
    else:
        if len(x) > 1:
            total_stars = float(x[:-1]) * num_map.get(x[-1].upper(), 1)
    return int(total_stars)
def human_format(num):
    num = float('{:.3g}'.format(num))
    magnitude = 0
    while abs(num) >= 1000:
        magnitude += 1
        num /= 1000.0
    return '{}{}'.format('{:f}'.format(num).rstrip('0').rstrip('.'), ['', 'K', 'M', 'B', 'T'][magnitude])


def result(Hit_rate):
    Hit_rate_readable = human_format(Hit_rate)
    st.subheader("Hitrate : " + str(Hit_rate))
    st.subheader("Compact Number Format: " + str(Hit_rate_readable))

def hitrate(url,pp):
    try:
        url = url.lower()
        if url.startswith('www') or url.startswith('http'):
            return st.success('Please enter the domain correctly. Eg: google.com')
        headers = {
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36',
            'accept': 'image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8',
            'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8'
        }

        response = requests.get(f'https://data.similarweb.com/api/v1/data?domain={url}', headers=headers)
        #print(response.status_code)
        if response.status_code == 200:
            j = response.text
            d = json.loads(j)
            if len(d) > 0:
                visit = d['Engagments'].get("Visits","")
                PagePerVisit = d['Engagments'].get("PagePerVisit","")
                visit = int(float(visit))
                PagePerVisit = float(PagePerVisit)
                hitrate = (visit*PagePerVisit)/30*(pp/100)
                return result(hitrate)
        elif response.status_code == 404 or response.status_code == 400:
            return st.success('No domain name exist!')
        else:
            return st.success('Something went wrong! Please let me know at: shritam.kumar@dataweave.com')
    except Exception as e:
        return st.success(f'Something went wrong! Please let me know at: shritam.kumar@dataweave.com')

def cycle_time_hitrate(url):
    try:
        url = url.lower()
        if url.startswith('www') or url.startswith('http'):
            return 1
        headers = {
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36',
            'accept': 'image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8',
            'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8'
        }

        response = requests.get(f'https://data.similarweb.com/api/v1/data?domain={url}', headers=headers)
        #print(response.status_code)
        if response.status_code == 200:
            j = response.text
            d = json.loads(j)
            if len(d) > 0:
                visit = d['Engagments'].get("Visits","")
                PagePerVisit = d['Engagments'].get("PagePerVisit","")
                visit = int(float(visit))
                PagePerVisit = float(PagePerVisit)
                hitrate = (visit*PagePerVisit)/30/10
                return hitrate
        elif response.status_code == 404 or response.status_code == 400:
            return 2
        else:
            return 3
    except Exception as e:
        return 4




magnitudeDict={0:'', 1:'Thousand', 2:'Million', 3:'Billion', 4:'Trillion', 5:'Quadrillion', 6:'Quintillion', 7:'Sextillion', 8:'Septillion', 9:'Octillion', 10:'Nonillion', 11:'Decillion'}
def simplify(num):
    num=floor(num)
    magnitude=0
    while num>=1000.0:
        magnitude+=1
        num=num/1000.0
    return(f'{floor(num*100.0)/100.0} {magnitudeDict[magnitude]}')


#hitrate from webpage
def hitrate_by_webpage(domain,pp):
  headers = {
    'authority': 'www.similarweb.com',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.54 Safari/537.36',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-user': '?1',
    'sec-fetch-dest': 'document',
    'sec-ch-ua': '"Google Chrome";v="95", "Chromium";v="95", ";Not A Brand";v="99"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Linux"',
    'referer': 'https://www.similarweb.com/',
    'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
  }

  response = requests.get(f'https://www.similarweb.com/website/{domain}/', headers=headers)
  tree = html.fromstring(response.text)
  values = tree.xpath('.//p[@class="wa-overview__engagement-value"]/text()')
  if len(values) ==4:
    total_visit = convert_str_to_number(values[0])
    PagePerVisit = float(values[2])
    hitrate = (total_visit*PagePerVisit)/30*(pp/100)
    return hitrate

if add_selectbox == 'Hitrate':
    st.title("DataWeave - Hit Rate Calculator")
    #st.header("**Enter the domain name: **")
    try:
        form_1 = st.form(key='my-form1')
        url = form_1.text_input("Enter the domain name:")
        pp = form_1.number_input("Politeness Policy %: ",10)
        submit = form_1.form_submit_button('Submit')

        if submit:
            st.header("**Result**")
            if pp>10:
                st.success(f"It's Recommended To Follow The Politeness Policy: 10%.")
            hit_web_page = hitrate_by_webpage(url,pp)
            if hit_web_page:
                result(hit_web_page)
            else:
                hitrate(url,pp)
    except IndexError:
        #submit = form.form_submit_button('Submit')
        st.success(f'Input more then a Trillion is out of scope!')
    except:
        #submit = form.form_submit_button('Submit')
        st.success(f'Please Enter Valid Input!!')


elif add_selectbox=='ETA':
    st.title("DataWeave - ETA Calculator")
    try:
        form_1 = st.form(key='my-form1')
        domain = form_1.text_input("Enter domain name:")
        unique_urls = form_1.text_input("Total unique urls: ")
        Extra_basic_req = form_1.number_input("Additional requests: ", min_value=0, format='%i')
        zip_codes = form_1.number_input("Number of Zip codes: ", min_value=0, format='%i')
        each_zip_codes = form_1.number_input("Number of additional requests per Zip codes: ", min_value=0, format='%i')
        Listing_crawls = form_1.slider(label='Days for Listing Crawls: ', min_value=0, max_value=30)
        slider_extra_request =  form_1.slider(label='Buffer days: ', min_value=0, max_value=30)
        project_start_date = form_1.date_input('Start Date:', today)
        submit = form_1.form_submit_button('Submit')

        if submit:
            hit_web_page = hitrate_by_webpage(domain,pp=10)
            if hit_web_page:
                return_cycle = hit_web_page
            else:
                return_cycle = cycle_time_hitrate(domain)
            #return_cycle = cycle_time_hitrate(domain)
            if return_cycle == 1:
                st.success('Please enter the domain correctly. Eg: google.com')
            elif return_cycle == 2:
                st.success('No domain name exist!')
            elif return_cycle == 4 or return_cycle == 3:
                st.success(f'Something went wrong! Please let me know at: shritam.kumar@dataweave.com')
            else:
                unique_urls = convert_str_to_number(unique_urls)
                hitrate_eta_ = return_cycle
                ETA = (unique_urls + unique_urls * Extra_basic_req + each_zip_codes *  zip_codes * unique_urls)/hitrate_eta_+Listing_crawls+slider_extra_request
                ETA_int = round(ETA)
                if ETA_int == 0:
                    ETA_int = 1
                days_after = (datetime.strptime(str(project_start_date), "%Y-%m-%d")+timedelta(days=ETA_int)).isoformat().split("T")[0]
                st.subheader(f'Hit-rate is: {human_format(round(hitrate_eta_))}')
                st.subheader(f'Your Cycle Time is: {ETA_int} Days.')
                st.subheader(f'Your Estimated Time of Arrival is: {days_after}')
    except IndexError:
        st.success(f'Input more then a Trillion is out of scope!')
    except:
        st.success(f'Please Enter Valid Input!!')

#Cache API Generator
elif add_selectbox=='Cache API Generator':
    st.title("DataWeave - Cache API Generator")

    col_1, col_2= st.columns([1,1])

    with col_1:
        input = st.text_area("Input JSON dump", "{}", height=600)

    with col_2:
        try:
            st.text("Cache api")
            case_data_holder = []
            j_out = json.loads(input)
            json_out = j_out.get("dump_files")
            #print(j_out)
            #print(type(j_out))

            for d in json_out:
                if d.get("status_code") ==200:
                    path = d.get("path")
                    urlh = path.split("/")[-1].replace(".warc.gz","")
                    crawl_type = path.split("/")[1]
                    crawl_date = path.split("/")[2]
                    hr = path.split("/")[3]
                    source = path.split("/")[4]
                    cache_api = f"http://api.cache.dweave.net/cache/?urlh={urlh}&crawl_date={crawl_date}&source={source}&crawl_type={crawl_type}&crawl_hour={hr}"
                    temp = {}
                    temp["api"] = cache_api
                    temp["tag"] = d.get("tag")
                    case_data_holder.append(temp)

            for tag in case_data_holder:

                st.write(f'**{tag.get("tag")}**')
                #st.text_area(tag.get("api"))
                st.write(f'{tag.get("api")}')
        except Exception as e:
            st.text_area("", e, height=200)

elif add_selectbox=='Count ⇆ Number':
    st.title("DataWeave - Count ⇆ Number")
    form = st.form(key='my-form')
    try:
        count_inp= form.text_input("Enter the Count/Number:", "1.5M")
        count = convert_str_to_number(str(count_inp))
        
        if count_inp.isdigit():
            c_to_n=human_format(int(count_inp))
        else:
            c_to_n=count
        Numeric_Format = f"{count:,}"
        Compact_Long = simplify(int(count))

        submit = form.form_submit_button('Submit')
        if submit:
            st.header("**Result**")
            st.subheader(f"Count ⇆ Number: {c_to_n}")
            st.subheader(f"Numeric Format: {Numeric_Format}")
            st.subheader(f"Compact Long: {Compact_Long}")
    except IndexError:
        submit = form.form_submit_button('Submit')
        st.success(f'Input more then a Trillion is out of scope!')
    except:
        submit = form.form_submit_button('Submit')
        st.success(f'Please Enter Valid Input!!')

elif add_selectbox=='URL Decoder':
    st.title("DataWeave - URL Decoder")
    form = st.form(key='my-decode')
    try:
        url= form.text_area("URL:")

        submit = form.form_submit_button('Submit')
        if submit:
            st.header("**Result**")
            
            st.code(f"{unquote(url)}")
    except:
        submit = form.form_submit_button('Submit')
        st.success(f'Please Enter Valid URL!!')

elif add_selectbox=="Sitemap Finder":
    st.title("DataWeave - Sitemap Finder")
    #st.success(f'Feature Under Construction!!')
    form = st.form(key='my-decode')
    
    try:
        domain_name= form.text_input("Enter the domain name:")
        if domain_name.startswith("www") or domain_name.startswith("http"):
            submit = form.form_submit_button('Submit')
            st.success('Please enter the domain correctly. Eg: tatacliq.com')
        else:
            submit = form.form_submit_button('Submit')
            if submit:
                headers = {
                'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36',
                'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
                }
                result = []
                #robot_sitemap = ''
                response = requests.get(f'https://www.{domain_name}/robots.txt', headers=headers)
                if response.status_code ==200:
                    resp_list = response.text.split("\n")
                    check = [i for i in resp_list if "sitemap" in i.lower()]
                    if check:
                        url = [i.lower() for i in resp_list if "Sitemap".lower() in i]
                        result.append(url[0].rsplit("sitemap:")[-1].strip())
                if result==[]:
                    # to search
                    query = f"site:{domain_name} filetype:xml"

                    result = []
                    for j in search(query):
                        #print(j)
                        result.append(j)
            # submit = form.form_submit_button('Submit')
            # if submit:
                st.header("**Result**")
                if len(result) != 0:
                    for i in result:
                        st.code(f"{i}")
                else:
                    st.success('No sitemap url found! Please check after sometime!!')

    except Exception as e:
        print(e)
        submit = form.form_submit_button('Submit')
        st.success(f'Please Enter a Valid domain name!!')

        



     


#footer
footer="""<style>
a:link , a:visited{
color: blue;
background-color: transparent;
text-decoration: underline;
}

a:hover,  a:active {
color: red;
background-color: transparent;
text-decoration: underline;
}

.footer {
position: fixed;
left: 0;
bottom: 0;
width: 100%;
background-color: white;
color: black;
text-align: center;
}
</style>
<div class="footer">
<p>Developed with ❤ by <a style='display: block; text-align: center;' href="https://twitter.com/shritamw" target="_blank">Shritam Kumar Mund</a></p>
</div>
"""
st.markdown(footer,unsafe_allow_html=True)



#remove hamburger 
hide_streamlit_style = """
            <style>
            #MainMenu {visibility: hidden;}
            footer {visibility: hidden;}
            </style>
            """
st.markdown(hide_streamlit_style, unsafe_allow_html=True) 